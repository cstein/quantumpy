"""
**********************************************************************
util.py

Copyright (C) 2011 Mikael W. Ibsen
Some portions Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
import os
import openbabel
import numpy

def is_string(input):
	return type(input) == type('a')

def file_exists(filename):
  if not is_string(filename):
    raise TypeError
  try:
    f = open(filename,'r')
  except IOError:
    return False
  f.close()
  return True

def file_extension(path_to_file):
	(filename,extension) = getFilenameAndExtension(path_to_file)
	return extension

def file_basename(path_to_file):
	(filename,extension) = getFilenameAndExtension(path_to_file)
	return filename

def getFilenameAndExtension(path_to_file):
	if not is_string(path_to_file):
		raise TypeError
	basename = os.path.split(path_to_file)[1]
	return os.path.splitext(basename)

def fileToMol(filename):
  file_format = OBGetFormatFromFilename(filename)
  obmol = OBMoleculeFromFilenameAndFormat(filename, file_format)
  OBCheckMoleculeConsistency(obmol)
  return obmol

def molToFile(obmol, filename, appendum=None):
  file_format = OBGetFormatFromFilename(filename)
  OBMoleculeToFilenameAndFormat(obmol, filename, file_format, appendum)

def OBGetFormatFromFilename(filename):
  return file_extension(filename)[1:]

def OBMoleculeFromFilenameAndFormat(filename, file_format):
  obc = openbabel.OBConversion()
  obc.SetInFormat(file_format)
  obmol = openbabel.OBMol()
  obc.ReadFile(obmol, filename)
  return obmol

def OBMoleculeToFilenameAndFormat(obmol,filename,file_format,appendum=None):
  obc = openbabel.OBConversion()
  obc.SetOutFormat(file_format)
  if appendum is not None:
    obc.AddOption("f", openbabel.OBConversion.OUTOPTIONS, appendum)
  #print "attempting to write file '%s'" % filename
  obc.WriteFile(obmol, filename)
  return obmol

def OBCheckMoleculeConsistency(obmol):
  if obmol.NumAtoms() < 1:
    raise ValueError("Molecule has no atoms.")

def OBGetCoordinates(obmol):
  iters = openbabel.OBMolAtomIter(obmol)
  atoms = [[atom.GetX(), atom.GetY(), atom.GetZ()] for atom in iters]
  return numpy.array(atoms)

def OBSetCoordinates(obmol, coords):
  obmol.BeginModify()
  for i,atom in enumerate(openbabel.OBMolAtomIter(obmol)):
    atom.SetVector(coords[i][0], coords[i][1], coords[i][2])
  obmol.EndModify()
  return obmol

def OBAtomTypes(obmol):
  iters = openbabel.OBMolAtomIter(obmol)
  res = [atom.GetAtomicNum() for atom in iters]
  return res

def OBAlignMolecules(obmol1, obmol2):
  align = openbabel.OBAlign(obmol1, obmol2)
  align.Align()
  align.UpdateCoords(obmol2)

def OBMolFromCoordinatesAndTypes(coords, types):
  obmol = openbabel.OBMol()
  obmol.BeginModify()
  for (i,c) in enumerate(coords):
    atom = openbabel.OBAtom()
    atom.SetAtomicNum(types[i])
    atom.SetVector(float(c[0]), float(c[1]), float(c[2]))
    obmol.AddAtom(atom)

  obmol.EndModify()
  obmol.ConnectTheDots()
  obmol.PerceiveBondOrders()
  return obmol
