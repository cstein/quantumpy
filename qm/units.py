"""
**********************************************************************
units.py

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
UNITS = dict()
UNITS['EV/HARTREE'] = 27.212
UNITS['KCALMOL/HARTREE'] = 627.51
UNITS['AANGSTROM/AU'] = 0.529177249
UNITS['AU/AANGSTROM'] = 1.0 / UNITS['AANGSTROM/AU']

MASS = dict()
MASS[1] = 1.00782
MASS[8] = 16.8
MASS[9] = 18.998403
MASS[18] = 35.453
