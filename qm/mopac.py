"""
**********************************************************************
mopac.py

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
from baseqm import BaseQM
from units import UNITS

# BASIS SET DEFINITION
BASIS = dict()
BASIS['AM1'] = 'AM1'
BASIS['RM1'] = 'RM1'
BASIS['PM3'] = 'PM3'
BASIS['PM6'] = 'PM6'

class Mopac(BaseQM):
  def __init__(self,obmol):
    BaseQM.__init__(self,obmol)
    self._basisSets = BASIS
  
  def _setupStandards(self):
    self.path = "/opt/mopac/MOPAC2009.exe"
    self.infile_extension = 'mop'
    self.outfile_extension = 'out'
    self.obformat = 'mop'
    self.command = '%s %s 2> /dev/null >> %s' # mopac makes to logfile itself

  def _makeHeader(self):
    try:
      basis = self.basis
    except AttributeError:
      raise AttributeError("Basis set not set")
    f = open("%s/%s" % (self.temporary_dir, self.obformat), "w")
    f.write("1SCF %s GRADIENT CHARGE=%i\n" % (self.basis, self.getTotalCharge()))
    f.close()

  def _parseLogfile(self):
    f = open(self.outfile, 'r')
    counter = 0
    for line in f:
  
      # parse the energy
      if "TOTAL ENERGY" in line:
        data = line.split()
        self.e = float(data[3]) / UNITS['EV/HARTREE']
  
      # parse the gradient
      if "       FINAL  POINT  AND  DERIVATIVES" in line:
        counter = 3*(self.getNumAtoms()+1)
  
      if counter > 0:
        counter -= 1
        if counter < 3*self.getNumAtoms():
          data = line.split()
          jdx = int(data[1])-1
          idx = int(data[0])-3*jdx-1
          self.g[jdx][idx] = float(data[6]) / (UNITS['KCALMOL/HARTREE']) * UNITS['AANGSTROM/AU']
  
    f.close()

if __name__ == '__main__':
  pass
